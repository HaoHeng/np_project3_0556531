#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<fcntl.h>

#include<sys/select.h>

//#include<netinet/in.h> // for bsd only.

#define CONNECTING 0
#define READING 1
#define WRITING 2
#define DONE 3

#define BUFFER_SIZE 10001

typedef
struct Server{
	char ip[20] ;
	char port[10] ;
	char test_data_name[100] ;
	char received_data[BUFFER_SIZE] ; // data received from servers
	char command[BUFFER_SIZE] ; // command read from testing data file.
	unsigned bytes_not_yet_sent ;
	int is_connect ;
	int status ;

	FILE* test_data_file ;

	int socketfd ;
}
Server ;

void parse_query_string(Server*,char*) ;
int connect_to_servers(Server* servers) ;

int main(){
	//printf("Content-type: text/html\n\n") ;
	//printf("Hello, this is a small hello :))\n") ;


	Server servers[5] ;
	parse_query_string(servers,getenv("QUERY_STRING")) ;

	/*
	int i ;
	for(i=0 ; i<5 ; i++){
		printf("ip %s port %s test data %s\n",servers[i].ip,servers[i].port,servers[i].test_data_name) ;
	}
	*/

	int number_of_connection = connect_to_servers(servers) ;


	printf("Content-type: text/html\n\n") ;

	printf("<html>\n") ;
	printf("<head>\n") ;
	printf("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n") ;
	printf("<title>Network Programming Homework 3</title>\n") ;
	printf("</head>\n") ;
	printf("<body bgcolor=#336699>\n") ;
	printf("<font face=\"Courier New\" size=2 color=#FFFF99>\n") ;
	printf("<table width=\"800\" border=\"1\">\n") ;
	printf("<tr>\n") ;
	printf("<td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>\n",servers[0].ip,servers[1].ip,servers[2].ip,servers[3].ip,servers[4].ip) ;
	printf("</tr>\n") ;
	printf("<tr>\n") ;
	printf("<td valign=\"top\" id=\"m0\"></td><td valign=\"top\" id=\"m1\"></td><td valign=\"top\" id=\"m2\"></td><td valign=\"top\" id=\"m3\"></td><td valign=\"top\" id=\"m4\"></td>\n") ;
	printf("</tr>\n") ;
	printf("</table>\n") ;

	fd_set rfds ;
	fd_set wfds ;
	fd_set rs ;
	fd_set ws ;

	int max_fd=0;

	FD_ZERO(&rfds) ;
	FD_ZERO(&wfds) ;
	FD_ZERO(&rs) ;
	FD_ZERO(&ws) ;

	// set active servers into rs and ws
	int i ;
	for(i=0 ; i<5 ; i++){
		if(servers[i].is_connect){
			FD_SET(servers[i].socketfd,&rs) ;
			FD_SET(servers[i].socketfd,&ws) ;
			if(servers[i].socketfd>max_fd) max_fd = servers[i].socketfd ;
		}
	}

	rfds = rs ;
	wfds = ws ;


	while(number_of_connection>0){ // there are still connections connecting to servers.

		memcpy(&rfds,&rs,sizeof(rfds)) ;
		memcpy(&wfds,&ws,sizeof(wfds)) ;

		select(max_fd+1,&rfds,&wfds,(fd_set*)0,(struct timeval*)0) ;

		int i ;
		for(i=0 ; i<number_of_connection ; i++){
			
			if(servers[i].is_connect==0) continue ;

			if(servers[i].status==CONNECTING && (FD_ISSET(servers[i].socketfd,&rfds) || FD_ISSET(servers[i].socketfd,&wfds))){
				servers[i].status=READING ;
				FD_CLR(servers[i].socketfd,&ws) ;
			}
			else if(servers[i].status==WRITING && FD_ISSET(servers[i].socketfd,&wfds)){
				
				fgets(servers[i].command,sizeof(servers[i].command),servers[i].test_data_file) ;
				
				char* new_line_found = strchr(servers[i].command,'\n') ;
				char* ptr = strchr(servers[i].command,'\r') ;

				int n=BUFFER_SIZE-1 , p=BUFFER_SIZE-1 , min ;
				if(new_line_found!=NULL) n = new_line_found - servers[i].command ;
				if(ptr!=NULL) p = ptr - servers[i].command ;
				if(n > p) min = p ;
				else min = n ;

				char s[BUFFER_SIZE] = "" ;
				strncpy(s,servers[i].command,min) ;

				
				// <script>document.all['m0'].innerHTML += "<b>ls</b><br>";</script>
				printf("<script>document.all['m%d'].innerHTML += \"<b>",i) ;
				//printf("%s",s) ;
				int j ;
				for(j=0 ; j<strlen(servers[i].command) ; j++){

					if(servers[i].command[j]=='<') printf("&lt") ;
					else if(servers[i].command[j]=='>') printf("&gt") ;
					else if(servers[i].command[j]=='\r') continue ;
					else if(servers[i].command[j]=='\n') printf("<br>") ;
					else if(servers[i].command[j]=='\"') printf("&quot") ;
					else printf("%c",servers[i].command[j]) ;
					
				}
				printf("</b><br>\";</script>\n") ;
				fflush(stdout) ;
				
				write(servers[i].socketfd,servers[i].command,strlen(servers[i].command)) ;
				
				if(new_line_found){
					// an newline is encountered, which means a command in a single line is sent completely.
					// --> change state.
					FD_CLR(servers[i].socketfd,&ws) ;
					servers[i].status = READING ;
					FD_SET(servers[i].socketfd,&rs) ;
					if(servers[i].socketfd > max_fd) max_fd = servers[i].socketfd ;
				}
				
				
			}
			else if(servers[i].status==READING && FD_ISSET(servers[i].socketfd,&rfds)){
				
				bzero(servers[i].received_data,sizeof(servers[i].received_data)) ;
				int n = read(servers[i].socketfd,servers[i].received_data,BUFFER_SIZE-1) ;
				if(n>0){
					printf("<script>document.all['m%d'].innerHTML += \"",i) ;
					fflush(stdout) ;
					int j ;
					// print received data. However, some special characters should be transferred before printing.
					// other normal characters are printed as what they are originally.
					for(j=0 ; j<strlen(servers[i].received_data) ; j++){

						if(servers[i].received_data[j]=='<') printf("&lt") ;
						else if(servers[i].received_data[j]=='>') printf("&gt") ;
						else if(servers[i].received_data[j]=='\r') continue ;
						else if(servers[i].received_data[j]=='\n') printf("<br>") ;
						else if(servers[i].received_data[j]=='\"') printf("&quot") ;
						else printf("%c",servers[i].received_data[j]) ;
						
					}
					printf("\";</script>\n") ;
					fflush(stdout) ;
					// <script>document.all['m0'].innerHTML += "** Welcome to the information server. **<br>";</script>

					char* pos = strchr(servers[i].received_data,'%') ;
					if(pos!=NULL){
						// server send a prompt for user.
						FD_CLR(servers[i].socketfd,&rs) ;
						servers[i].status = WRITING ;
						FD_SET(servers[i].socketfd,&ws) ;
						if(servers[i].socketfd>max_fd) max_fd = servers[i].socketfd ;
					}
				}
				else if(n==0){
					FD_CLR(servers[i].socketfd,&rs) ;
					servers[i].status = DONE ;
					servers[i].is_connect = 0 ;
					number_of_connection-- ;
				}
				else{
					continue ;
				}
				
			}


		}

	}

	printf("</font></body></html>\n") ;
}

void parse_query_string(Server* servers, char* q){
	//h1=123&p1=456&f1=777&h2=&p2=&f2=&h3=&p3=&f3=&h4=&p4=&f4=&h5=&p5=&f5=
	int i ;
	char query_string[1000] = "" ;
	strcpy(query_string,q) ;
	strcat(query_string,"&") ;

	char* parse_from = query_string ;
	
	for(i=0 ; i<5 ; i++){
		char ip[20]="" ;
		char port[10]="" ;
		char test_data_name[100]="" ;

		// parse for ip
		char* start = strchr(parse_from,'=') ;
		char* end = strchr(start,'&') ;
		start++ ;
		strncpy(ip,start,end-start) ;

		// parse for port
		start = strchr(end,'=') ;
		end = strchr(start,'&') ;
		start++ ;
		strncpy(port,start,end-start) ;

		// parse for testing data
		start = strchr(end,'=') ;
		end = strchr(start,'&') ;
		start++ ;
		strncpy(test_data_name,start,end-start) ;



		bzero(servers[i].ip,sizeof(servers[i].ip)) ;
		bzero(servers[i].port,sizeof(servers[i].port)) ;
		bzero(servers[i].test_data_name,sizeof(servers[i].test_data_name)) ;

		strcpy(servers[i].ip,ip) ;
		strcpy(servers[i].port,port) ;
		strcpy(servers[i].test_data_name,test_data_name) ;
		if(strcmp(servers[i].ip,"")==0){
			servers[i].is_connect = 0 ;
		}
		else{
			servers[i].is_connect = 1 ;
		}

		parse_from = end ;


	}

}

int connect_to_servers(Server* servers){
	int i , number_of_connections=0 ;

	for(i=0 ; i<5 ; i++){
		
		if( ! servers[i].is_connect ) continue ;

		struct sockaddr_in server_addr ;
		struct hostent* he = gethostbyname(servers[i].ip) ;
		int server_socket = socket(AF_INET,SOCK_STREAM,0) ;
		bzero(&server_addr,sizeof(server_addr)) ;
		server_addr.sin_family = AF_INET ;
		server_addr.sin_port = htons(atoi(servers[i].port)) ;
		inet_pton(AF_INET,inet_ntoa(*(struct in_addr*)he->h_addr), &server_addr.sin_addr) ;
		int flag = fcntl(server_socket,F_GETFL,0) ;
		fcntl(server_socket,F_SETFL,flag|O_NONBLOCK) ;

		connect(server_socket,(struct sockaddr*)&server_addr, sizeof(server_addr)) ;

		FILE* f = fopen(servers[i].test_data_name,"r") ;
		
		servers[i].socketfd = server_socket ;
		servers[i].status = CONNECTING ;
		servers[i].test_data_file = f ;
		servers[i].bytes_not_yet_sent = 0 ;

		number_of_connections++ ;

	}

	return number_of_connections ;
}

