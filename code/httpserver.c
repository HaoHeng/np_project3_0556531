#include<stdio.h>
#include<stdlib.h> // exit
#include<unistd.h> // fork, pid_t, write
#include<sys/wait.h> // wait
#include<string.h> // strlen
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 10001

void terminated_child_process_collector() ;
int create_server_socket(int port) ;

int main(int argc, char* argv[]){

	signal(SIGCHLD,terminated_child_process_collector) ;

	int server = create_server_socket(argc==1?16017:atoi(argv[1])) ;

	while(1){

		int connection = accept(server,(struct sockaddr*)NULL,NULL) ;

		char http_request[BUFFER_SIZE] = "" ;
		read(connection,http_request,BUFFER_SIZE-1) ;

		
		//printf("%s",http_request) ; 
		// GET /form_get.htm HTTP/1.1 ...
		// GET /cgi-bin/greetings.cgi?h1=140.113.168.191&p1=11382&f1=f1&h2=&p2=&f2=&h3=&p3=&f3=&h4=&p4=&f4=&h5=&p5=&f5= HTTP/1.1
		char* start = strchr(http_request,' ') ;
		start++ ; // skip space
		start++ ; // skip forward slash.

		char* end1 = strchr(start,' ') ;
		char* end2 = strchr(start,'?') ;
		char* end ;
		if(end2==NULL) end = end1 ; // end1 must not be NULL, because there must be space to be found.
		else{
			int n1 = end1 - start ;
			int n2 = end2 - start ;
			if(n1 > n2) end = end2 ;
			else end = end1 ;
		}


		char file_name[10001] = "" ;
		strncpy(file_name,start,end-start) ;

		char* file_extension = strchr(file_name,'.') ; // file type
		if(strncmp(file_extension,".cgi",4)==0){

			write(connection,"HTTP/1.0 200 OK\n",strlen("HTTP/1.0 200 OK\n")) ;
			
			char query_string[1000] = "" ;
			start = end + 1 ;
			end = strchr(start,' ') ;
			strncpy(query_string,start,end-start) ;

			setenv("QUERY_STRING",query_string,1) ;
			setenv("CONTENT_LENGTH","777",1) ;
			setenv("REQUEST_METHOD","GET",1) ;
			setenv("SCRIPT_NAME","script",1) ;
			setenv("REMOTE_HOST","your computer",1) ;
			setenv("REMOTE_ADDR","don't ask me.",1) ;
			setenv("AUTH_TYPE","of course no.",1) ;
			setenv("REMOTE_USER","you",1) ;
			setenv("REMOTE_IDENT","you",1) ;


			pid_t pid = fork() ;
			if(pid<0){
				printf("fork failed.\n") ;
				exit(1) ;
			}
			else if(pid==0){

				dup2(connection,STDIN_FILENO) ;
				dup2(connection,STDOUT_FILENO) ;
				dup2(connection,STDERR_FILENO) ;

				close(connection) ;
				close(server) ;
				execl(file_name,file_name,NULL) ;
				printf("exec fail\n") ;
				exit(1) ;

			}
			close(connection) ;
		}
		else if(strncmp(file_extension,".htm",4)==0){

			char response[BUFFER_SIZE] = "" ;
			char html_content[BUFFER_SIZE] = "" ;
			strcpy(response,"HTTP/1.0 200 ok\n") ;
			strcat(response,"Content-type: text/html\n\n") ;
			printf("%s",response) ;
			write(connection,response,strlen(response)) ;

			FILE* f = fopen(file_name,"r") ;
			fread(html_content,sizeof(char),BUFFER_SIZE-1,f) ;
			write(connection,html_content,strlen(html_content)) ;

			fclose(f) ;
			close(connection) ;

		}
		//else{
		//	printf("file type error\n") ;
			//exit(1) ;
		//}

	}

	


	close(server) ;
	/*
	while(1){

		int connection = accept(server,(struct sockaddr*)NULL,NULL) ;

		pid_t pid = fork() ;
		if(pid<0){
			printf("fork failed.\n") ;
		}
		else if(pid==0){
			close(server) ;
			exit(0) ;
		}
		else{
			printf("this is parent, child pid is %d.\n",pid) ;
			close(connection) ;
		}
	}
	*/


}

void terminated_child_process_collector(){
	int status ;
	pid_t pid = wait3(&status,WNOHANG,(struct rusage*)0) ;
	printf("pid %d collected.\n",pid) ;
	fflush(stdout) ;
}

int create_server_socket(int port){
	int server ;
	struct sockaddr_in server_addr ;

	server = socket(AF_INET, SOCK_STREAM, 0) ;
	
	bzero( &server_addr, sizeof(server_addr));
	
	server_addr.sin_family = AF_INET ;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY) ;
	server_addr.sin_port = htons(port) ;

	if( bind(server,(struct sockaddr*)&server_addr,sizeof(server_addr)) == -1 ){
		printf("bind error.\n") ;
		exit(1) ;
	}

	listen(server,20) ;

	return server ;
}
